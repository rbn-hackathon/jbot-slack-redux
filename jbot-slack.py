#!/usr/bin/python
"""This will be a bot that connects to slack and 
mirrors what my IRC jbot does, only a little less vulgar"""
#import important libraries for connections
import time, os, urls
from json import loads, dumps
from slackclient import SlackClient
#set up the connection itself
token = os.environ['SLACK_API_KEY'] # found at https://api.slack.com/web#authentication
sc = SlackClient(token)
if sc.rtm_connect():
    while True:
        m = sc.rtm_read()
        if (len(m) > 0 and 'text' in m[0]):
        	D = m[0]['text']
        	if (D.split()[0] == "!number"):
        		sc.api_call("chat.postMessage", channel="#hack-a-thon", text=urls.numbers(D), username="jbot-slack", icon_emoji='blackmage')
        time.sleep(1)
else:
    print "Connection Failed, invalid token?"
